# Tumor_classification_ML


## Name
Ml model testing for RNAseq cancer classification dataset

## Description
This script has for objective to evaluate different machine learning models for the classification of a RNA-Seq Data Set containing information of 5 different types of cancer, breast cancer (BRCA), Colon Adenocarcinoma (COAD), Kidney Renal Clear Cell Carcinoma (KIRC), Lung Adenocarcinoma (LUAD) and Prostate Adenocarcinoma (PRAD). The selected models for testing are Multiclass logistic regression, Naive Bayes, Random forest and support vector machine.


## Installation
1) Download the repository to you local machine.
2) Install and deploy the conda enviroment (conda_enviroment.yaml) file present on the repository


## Usage
1) Enter into the downloaded folder and open a terminal
2) type python3 main.py
3) The differents graphs and plots will be created into the output folder of each one of the corresponding model

## Support
for any question or suggestion, contact me to gmoralest@gmail.com


## Authors and acknowledgment
Morales Gabriel - AMU Bioinformatics DLAD M2 student 2023. 


## Project status
The project is finish.
