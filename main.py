import lib.data_load as dataload
import lib.exploratory_pca as exppca
import lib.data_exploration as explore
import lib.data_splitting as split
import lib.SMOTE as smote
import lib.logistic_regression as logreg
import lib.support_vector_machine as svglib
import lib.model_target_prediction as prediction_metrics
import lib.randomforest as randomforest
import lib.naivebayes as NB

if __name__ == "__main__":
     # # # # # Data loading and object serialization # # # # #
     X_data, Y_labels = dataload.data_reading()

     # # # # # # Exploratory data analysis # # # # #
     explore.count_plot(Y_labels, "-Original data-")
     exppca.pca(X_data,Y_labels,"-Original data-")

     # # # # # # Data split into training (70%) and test (30%) # # # # #
     X_train, X_test, y_train, y_test = split.data_split(X_data, Y_labels)
     # explore.count_plot(y_train, "-Train data-")
     # exppca.pca(X_train,y_train,"-Train data-")

     # # # # # # # Balance feature training data if necessary # # # # #
     # # X_train_oversampled, y_train_oversampled = smote.synthetic_minority_oversampling_technique(X_train, y_train)
     # # explore.count_plot(y_train_oversampled, "-Oversamples data - SMOTE -")
     # # exppca.pca(X_train_oversampled,y_train_oversampled, "-Oversamples data - SMOTE -")
     # # # # # # # Feature training data scaling # # # # #
     # # # # # # # Feature data PCA dimensional reduction # # # # #
     
     
     # # # # # # Classification model development and optimization # # # # #
     ####logistig regression ######
     # # # # # Classification model fitting # # # # #
     logistic_regression_model = logreg.logistic_regression_model_fit(X_train, y_train)
     # # # # # # Model target prediction # # # # #
     Y_prediction = logreg.logistic_regression_prediction(logistic_regression_model,X_test)
     logistic_regression_cross_validate_predicted = logreg.logistic_regression_cross_validation(logistic_regression_model,X_test, y_test)
     # # # # # # Classification metrics calculation for validation and test data # # # # #
     prediction_metrics.roc_auc_curve(X_train, y_train,X_test, y_test, logistic_regression_model, "logistic_regression")
     prediction_metrics.model_target_prediction(y_test, Y_prediction, "logistic_regression")
     prediction_metrics.model_target_prediction(y_test, logistic_regression_cross_validate_predicted, "logistic_regression_+_cross_validation") 
     
     # # #### random forest #####
     # # # # # # Classification model fitting # # # # #
     X_train, X_test, y_train, y_test = split.data_split(X_data, Y_labels)
     random_forest_model = randomforest.random_forest_model_fit(X_train, y_train)
      # # # # # # # Model target prediction # # # # #
     Y_prediction = randomforest.random_forest_prediction(random_forest_model,X_test)
     random_forest_cross_validate_predicted = randomforest.random_forest_cross_validation(random_forest_model,X_test, y_test)
     # # # # # # # Classification metrics calculation for validation and test data # # # # #
     prediction_metrics.roc_auc_curve(X_train, y_train,X_test, y_test, random_forest_model, "Random_Forest")
     prediction_metrics.model_target_prediction(y_test, Y_prediction, "Random_Forest")
     prediction_metrics.model_target_prediction(y_test, random_forest_cross_validate_predicted, "Random_Forest_+_cross_validation") 


     # # #### Support Vector machine #####
     # # # # # # Classification model fitting # # # # #
     X_train, X_test, y_train, y_test = split.data_split(X_data, Y_labels)
     svg_model = svglib.svm_model_fit(X_train, y_train)
      # # # # # # # Model target prediction # # # # #
     Y_prediction = svglib.svg_prediction(svg_model,X_test)
     svg_cross_validate_predicted = svglib.svm_cross_validation(svg_model,X_test, y_test)
     # # # # # # # Classification metrics calculation for validation and test data # # # # #
     #prediction_metrics.roc_auc_curve(X_train, y_train,X_test, y_test, svg_model, "Support_vector_machine")
     prediction_metrics.model_target_prediction(y_test, Y_prediction, "Support_vector_machine")
     prediction_metrics.model_target_prediction(y_test, svg_cross_validate_predicted, "Support_vector_machine_+_cross_validation") 


     # # #### Naive Bayes GaussianNB() #####
     # # # # # # Classification model fitting # # # # #
     X_train, X_test, y_train, y_test = split.data_split(X_data, Y_labels)
     NB_model = NB.NB_model_fit(X_train, y_train)
      # # # # # # # Model target prediction # # # # #
     Y_prediction = NB.NB_model_prediction(NB_model,X_test)
     NB_cross_validate_predicted = NB.NB_model_cross_validation(NB_model,X_test, y_test)
     # # # # # # # Classification metrics calculation for validation and test data # # # # #
     prediction_metrics.roc_auc_curve(X_train, y_train,X_test, y_test, NB_model, "Naive_Bayes")
     prediction_metrics.model_target_prediction(y_test, Y_prediction, "Naive_Bayes")
     prediction_metrics.model_target_prediction(y_test, svg_cross_validate_predicted, "Naive_Bayes_+_cross_validation") 