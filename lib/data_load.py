import pandas as pd


def data_reading():
    # Loading data
    df_data = pd.read_csv('data/TCGA-PANCAN-HiSeq-801x20531/data.csv')
    df_labels = pd.read_csv('data/TCGA-PANCAN-HiSeq-801x20531/labels.csv')

    # Data preparation
    X = df_data.iloc[:,1:]
    Y = df_labels.iloc[:,1]
    return X, Y
