import matplotlib.pyplot as plt
from collections import Counter


def count_plot(labels, title):
    print("COUNTS", Counter(labels))
    labels.value_counts().plot(kind="bar")
    plt.xlabel("Cancer type",  size = 10)
    plt.ylabel("Count", size = 10)
    plt.title("Count per cancer type " + title, size = 15)
    plt.savefig('output/value_counts '+ title)
    plt.clf()
    plt.close()