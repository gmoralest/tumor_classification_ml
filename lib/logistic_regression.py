from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import cross_val_predict



def logistic_regression_model_fit(X_train, y_train):
    logreg = LogisticRegression(max_iter = 2500, C=1000000, random_state=0, multi_class='multinomial')
    logreg.fit(X_train,y_train)
    return logreg    

def logistic_regression_prediction(logreg, X_test):
    y_pred = logreg.predict(X_test)
    return y_pred

def logistic_regression_cross_validation(logreg, X,y):
    logistic_regression_cross_validate_predicted = cross_val_predict(logreg, X, y, cv=10)
    return logistic_regression_cross_validate_predicted

def logistic_regression_roc_auc_scores(logreg, X_test):
    y_scores = logreg.predict_proba(X_test)[:,1]
    return y_scores
