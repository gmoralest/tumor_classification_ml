from sklearn.metrics import confusion_matrix, ConfusionMatrixDisplay
from sklearn.metrics import classification_report,accuracy_score, recall_score, f1_score
from sklearn.metrics import roc_curve, roc_auc_score
import seaborn as sns
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from sklearn.preprocessing import LabelBinarizer
from sklearn.metrics import RocCurveDisplay


def model_target_prediction(y_test,y_pred,condition):    
    print("############ " + condition + " ############")
    confusion_matrix_prediction = confusion_matrix(y_test,y_pred)
    print(confusion_matrix_prediction)
    ConfusionMatrixDisplay(confusion_matrix_prediction).plot()
    print(classification_report(y_test,y_pred))
    sns.heatmap(pd.crosstab(y_test,y_pred), annot = True)
    print("accuracy score " +  str(accuracy_score(y_test,y_pred)))
    print("F1 score " + str(f1_score(y_test,y_pred,average = "macro")))
    print("recall score" + str(recall_score(y_test,y_pred,average = "macro")))
    plt.savefig('output/' + condition + "/heatmap" + condition)
    plt.close()

def roc_auc_curve(X_train, y_train,x_test, y_test, model, condition):
    y_score = model.fit(X_train, y_train).predict_proba(x_test)
    label_binarizer = LabelBinarizer().fit(y_train)
    y_onehot_test = label_binarizer.transform(y_test)
    classes_of_interest = ["BRCA","COAD","KIRC","LUAD","PRAD"]
    for class_of_interest in classes_of_interest:
        class_id = np.flatnonzero(label_binarizer.classes_ == class_of_interest)[0]
        RocCurveDisplay.from_predictions(
        y_onehot_test[:, class_id],
        y_score[:, class_id],
        name=f"{class_of_interest} vs the rest",
        color="darkorange",)
        plt.plot([0, 1], [0, 1], "k--", label="chance level (AUC = 0.5)")
        plt.axis("square")
        plt.xlabel("False Positive Rate")
        plt.ylabel("True Positive Rate")
        plt.title("One-vs-Rest ROC curves: " +  class_of_interest + "vs " + "(COAD & KIRC & LUAD & PRAD)" + condition)
        plt.savefig("output/" + condition +"/" + "ROC_curves" +  class_of_interest)
        plt.legend()
        
