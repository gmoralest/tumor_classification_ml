from sklearn.naive_bayes import GaussianNB
from sklearn.model_selection import cross_val_predict

#split data

#implement model

def NB_model_fit(X_train, y_train):
    NB_model = GaussianNB(var_smoothing=1e-09)
    NB_model.fit(X_train,y_train)
    return NB_model    

def NB_model_prediction(NB_model, X_test):
    y_pred = NB_model.predict(X_test)
    return y_pred

def NB_model_cross_validation(NB_model, X,y):
    NB_model_cross_validate_predicted = cross_val_predict(NB_model, X, y, cv=10)
    return NB_model_cross_validate_predicted