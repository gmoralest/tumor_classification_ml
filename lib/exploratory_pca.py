import pandas as pd
import numpy as np
from sklearn.decomposition import PCA
import matplotlib.pyplot as plt

from sklearn.preprocessing import StandardScaler

def pca(data, labels, title):
    x = StandardScaler().fit_transform(data)
    pca = PCA(n_components=10)

    principalComponents = pca.fit_transform(x)

    principalDataframe = pd.DataFrame(data = principalComponents, columns = ['PC1', 'PC2','PC3', 'PC4','PC5', 'PC6','PC7', 'PC8','PC9', 'PC10'])
    newDataframe = pd.concat([principalDataframe, labels],axis = 1)
    percent_variance = np.round(pca.explained_variance_ratio_* 100, decimals =2)
    columns = ['PC1', 'PC2','PC3', 'PC4','PC5', 'PC6','PC7', 'PC8','PC9', 'PC10']
    
    ##### SCATTER PLOT  ######
    
    plt.bar(x= range(1,11), height=percent_variance, tick_label=columns)
    plt.ylabel('Percentate of Variance Explained' )
    plt.xlabel('Principal Component')
    plt.title('PCA Scree Plot ' + title)
    plt.savefig('output/PCA_screeplot ' + title)
    plt.close()


    ####  PC1 VS PC2  ####

    plt.scatter(principalDataframe.PC1, principalDataframe.PC2)
    plt.title('PC1 against PC2')
    plt.xlabel('PC1')
    plt.ylabel('PC2')
    
    
    fig = plt.figure(figsize = (8,8))
    ax = fig.add_subplot(1,1,1) 
    ax.set_xlabel('PC1')
    ax.set_ylabel('PC2')

    ax.set_title('Plot of PC1 vs PC2 ' + title, fontsize = 20)

    targets = ['PRAD','LUAD','BRCA','KIRC','COAD']

    colors = ['#1f77b4', '#ff7f0e', '#2ca02c', '#d62728', '#9467bd']

    for target, color in zip(targets,colors):
        indicesToKeep = newDataframe['Class'] == target
        ax.scatter(newDataframe.loc[indicesToKeep, 'PC1']
                , newDataframe.loc[indicesToKeep, 'PC2']
                , c = color
                , s = 50)
        
    ax.legend(targets)
    ax.grid()
    fig.savefig('output/Plot of PC1 vs PC2 ' + title)
    print("explained_variance_ratio: ")
    print(pca.explained_variance_ratio_)
    ax.clear()
    plt.close()

    
    