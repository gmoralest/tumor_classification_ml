import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split
from sklearn import linear_model
from numpy import *
import math
import matplotlib.pyplot as plt


def data_split(X, Y):
    X_train, X_test, y_train, y_test = train_test_split(X,Y,test_size=0.30, random_state = 10,  stratify=Y)
    return(X_train, X_test, y_train, y_test)