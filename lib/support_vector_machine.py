from sklearn import svm
from sklearn.model_selection import cross_val_predict

def svm_model_fit(X_train, y_train):
    svg_model = svm.SVC(kernel='linear')
    svg_model.fit(X_train,y_train)
    return svg_model

def svg_prediction(svg_model, X_test):
    y_pred = svg_model.predict(X_test)
    return y_pred

def svm_cross_validation(svg_model, X,y):
    svg_cross_validate_predicted = cross_val_predict(svg_model, X, y, cv=10)
    return svg_cross_validate_predicted