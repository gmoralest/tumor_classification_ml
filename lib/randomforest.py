from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import cross_val_predict

#Create a Gaussian Classifier
def random_forest_model_fit(X_train,y_train):

    random_forest=RandomForestClassifier(n_estimators=100)
    random_forest.fit(X_train,y_train)
    return random_forest

def random_forest_prediction(random_forest, X_test):
    y_pred=random_forest.predict(X_test)
    return y_pred

def random_forest_cross_validation(random_forest, X,y):
    random_forest_cross_validate_predicted = cross_val_predict(random_forest, X, y, cv=10)
    return random_forest_cross_validate_predicted
