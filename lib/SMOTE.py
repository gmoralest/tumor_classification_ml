from imblearn.over_sampling import SMOTE

from collections import Counter

def synthetic_minority_oversampling_technique(X_train, y_train):
    counter_sample_values= Counter(y_train)
    #oversample = SMOTE(sampling_strategy= ({'BRCA': counter_sample_values['BRCA'], 'KIRC': counter_sample_values['BRCA'], 'LUAD': counter_sample_values['BRCA'], 'PRAD': counter_sample_values['BRCA'], 'COAD': counter_sample_values['BRCA']}), k_neighbors=100)
    #X_train_oversampled, y_train_oversampled = oversample.fit_resample(X_train, y_train)
    oversample = SMOTE(random_state=0)
    X_train_oversampled, y_train_oversampled = oversample.fit_resample(X_train, y_train)
    return X_train_oversampled, y_train_oversampled